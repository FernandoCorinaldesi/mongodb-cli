const { MongoClient } = require('mongodb');//injectamos la driver para la conexion
const fs = require('fs');//
const Json2csvParser = require('json2csv').Parser;//inyectamos el modulo para parsear de json a csv

async function convert() {

    const uri = "mongodb://localhost:27017";
    const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });
    try {
        // Coneccion a MongoDB 
        await client.connect();
        console.log('Conexion a la db exitosa');
        await converToCsv(client);
   } catch (e) {
        console.error(e);
   } finally {
        await client.close();
   }
}

async function converToCsv(client) {
    result = await client.db("persona").collection("personas").find().toArray();
    const csvFields = ['_id','nombre','apellido','edad'];
    const json2csvParser = new Json2csvParser({ csvFields });
    const csv = json2csvParser.parse(result);
    console.log("personas en formato csv:");
    console.log(csv);
          
    fs.writeFile('personas.csv', csv, function(err) {//escribo el archivo csv en la raiz
      if (err) throw err;
      console.log('archivo grabado');
    });
  };
  module.exports=convert;
 