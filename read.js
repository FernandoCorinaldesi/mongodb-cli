const { MongoClient } = require('mongodb');//injectamos la driver para la conexion

async function read() {
    const uri = "mongodb://localhost:27017";
    const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });
    try {
        // Coneccion a MongoDB 
        await client.connect();
        console.log('Conexion a la db exitosa');
        // llamadas a las funciones
        await showDocuments(client);

    } catch (e) {
        console.error(e);
    } finally {
        await client.close();
    }
}

async function showDocuments(client) {
    result = await client.db("persona").collection("personas").find();
    console.log("lista de personas:");
    await result.forEach((e) => console.log(e));
};
module.exports=read;