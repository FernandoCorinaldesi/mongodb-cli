
const { MongoClient } = require('mongodb');//injectamos la driver para la conexion

async function insert(persona) {
    const uri = "mongodb://localhost:27017";
    const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });
    try {
        // Coneccion a MongoDB 
        await client.connect();
        console.log('Conexion a la db exitosa');
        await insertDocument(client,persona);
     
    } catch (e) {
        console.error(e);
   } finally {
        await client.close();
    }
}
async function insertDocument(client,newDoc){
    const result = await client.db("persona").collection("personas").insertOne(newDoc);
    console.log(`Nuevo documento creado con el siguiente id: ${result.insertedId}`);
};

module.exports=insert;
