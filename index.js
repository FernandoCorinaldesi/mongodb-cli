#!/usr/bin/env node
//recordar usar npm link para poder usarlo desde cualquier directorio,
const program = require('commander');
const listdb = require('./listdb');
const insert = require('./insert');
const read = require('./read');
const convert = require('./convert');
const del = require('./delete');
const {prompt} = require('inquirer');
const chalk = require('chalk');
const figlet = require('figlet');

function main() {
    
    program.version('1.0.0')
        .description(chalk.green(figlet.textSync('Fer-Mongo-CLI', { horizontalLayout: 'full' })))

    program.command('verdb')
        .description('Muestra las db')
        .action(() => {
            listdb();
        });
    program.command('mostrar')
        .description('Muestra la coleccion personas')
        .action(() => {
            read();
        });
    program.command('convertir')
        .description('Convierte la coleccion personas en csv')
        .action(() => {
            convert();
        });
        program.command('borrar')
        .description('Borra el documento del id ingresado')
        .action(() => {
            prompt(loadDelQuestions()).then(respuesta=>del(respuesta));
        });
    program.command('insertar')
        .description('Inserta un documento en la coleccion personas')
        .action(() => {
            prompt(loadInsertQuestions()).then(respuesta=>insert(respuesta));
        });
    program.parse(process.argv);
}
main();

function loadInsertQuestions(){
    const question=[
        {
           type :'input',
           name : 'nombre',
           message: 'ingresar el nombre'
        },
        {
            type :'input',
            name : 'apellido',
            message: 'ingresar el apellido'
         },
         {
            type :'input',
            name : 'edad',
            message: 'ingresar la edad'
         }
    ]
    return question;
}
function loadDelQuestions(){
    const question=[
        {
           type :'input',
           name : '_id',
           message: 'ingresar el id'
        }
    ]
    return question;
}

