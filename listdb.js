const { MongoClient } = require('mongodb');//injectamos la driver para la conexion

async function listdb() {
    const uri = "mongodb://localhost:27017";
    const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });
    try {
        // Coneccion a MongoDB 
        await client.connect();
        console.log('Conexion a la db exitosa');
        // llamadas a las funciones
        await listDatabases(client);
                        
    } catch (e) {
        console.error(e);
   } finally {
        await client.close();
   }
}
async function listDatabases(client) {
    databasesList = await client.db().admin().listDatabases();
    console.log("Databases list:");
    //databasesList.databases.forEach(e => console.log(e));
    databasesList.databases.forEach(db => console.log(` - ${db.name}`));
};
module.exports=listdb;