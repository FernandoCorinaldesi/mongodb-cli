const { MongoClient } = require('mongodb');//injectamos la driver para la conexion

async function maindelete(id) {
    const uri = "mongodb://localhost:27017";
    const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });
    try {
        // Coneccion a MongoDB 
        await client.connect();
        console.log('Conexion a la db exitosa');
        // llamadas a las funciones
        await deleteListingById(client,id);
                        
    } catch (e) {
        console.error(e);
   } finally {
        await client.close();
   }
}

async function deleteListingById(client,id) {
    console.log(id);
    result = await client.db("persona").collection("personas").deleteOne(id[0]);
    console.log(`${result.deletedCount} el documento(s) fue borrado.`);
}
module.exports=maindelete;